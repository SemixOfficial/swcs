AddCSLuaFile()

-- econ as in skins and shit ;)

local ak_neon_rider = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/custom/ak_neon_rider",
    wearvalue = 1
})
local ak_uv = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/uvs/weapon_ak47",
    wearvalue = 1
})

local css_uv = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/uvs/weapon_knife_css",
    wearvalue = 1
})

local awp_cat = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/anodized_multi/workshop/awp_pawpaw",
    wearvalue = 1
})
local awp_medusa = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/antiqued/medusa_awp",
    wearvalue = 1
})

local deagle_etched = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/antiqued/etched_deagle",
    wearvalue = 1
})

local cz_etched = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/antiqued/etched_cz75",
    normalmap = "models/weapons/customization/paints/antiqued/etched_cz75_normal",
    wearvalue = 1
})

local bayonet_future = swcs.GenerateEconTexture({
    basetexture = "models/weapons/customization/paints/gunsmith/bayonet_future_alt",
    normalmap = "models/weapons/customization/paints/gunsmith/bayonet_future_alt_normal",
    wearvalue = 1
})

function SWEP:ApplyWeaponSkin(vm, owner)
    if SERVER then return end

    if not self.m_clSkinTexture then
        --local want_skin = owner:GetPData("swcs_skin")
        --if want_skin == nil then return end
        --print(vm, self.m_clSkinTexture, want_skin)

        self.m_clSkinTexture = "!" .. ak_neon_rider
    end
    if not self.m_clSkinTexture then return end

    --vm:SetSubMaterial(0, self.m_clSkinTexture)
end

function SWEP:RemoveWeaponSkin(vm, owner)
    vm:SetSubMaterial(0)
end
