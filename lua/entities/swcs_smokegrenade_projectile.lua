AddCSLuaFile()

ENT.Base = "baseswcsgrenade_projectile"
ENT.m_flTimeToDetonate = 1.5
ENT.m_flLastBounce = 0.0
ENT.m_bSmokeEffectSpawned = false

DEFINE_BASECLASS(ENT.Base)

local GRENADE_MODEL = "models/weapons/csgo/w_eq_smokegrenade_thrown.mdl"

function ENT:SetupDataTables()
    BaseClass.SetupDataTables(self)

    self:NetworkVar("Bool", 0, "DidSmokeEffect")
    self:NetworkVar("Int", 2, "SmokeEffectTickBegin")

    if CLIENT then
        self:NetworkVarNotify("SmokeEffectTickBegin", self.OnDataChanged)
        self:NetworkVarNotify("DidSmokeEffect", self.OnDataChanged)
    end
end

function ENT:Create(pos, angs, vel, angvel, owner)
    self:SetPos(pos)
    self:SetAngles(angs)

    self:SetVelocity(vel)
    self:SetInitialVelocity(vel)
    self:SetThrower(owner)
    self:SetOwner(owner)

    self:SetLocalAngularVelocity(angvel)
    self:SetFinalAngularVelocity(angvel)
    self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)

    return self
end

function ENT:Initialize()
    self:SetModel(GRENADE_MODEL)

    self:SetDetonateTimerLength( self.m_flTimeToDetonate )

    BaseClass.Initialize(self)
end

ENT.m_smokeParticleEffect = NULL
function ENT:SpawnSmokeEffect()
    if ( not self.m_bSmokeEffectSpawned ) then
        self.m_bSmokeEffectSpawned = true

        local vOrigin = self:GetNetworkOrigin()

        local pSmokeEffect = CreateParticleSystem(self, "explosion_smokegrenade", 0, 0, vector_origin)

        if pSmokeEffect then
            self.m_smokeParticleEffect = pSmokeEffect
            pSmokeEffect:SetSortOrigin( vOrigin )
            pSmokeEffect:SetControlPoint( 0, vOrigin )
            pSmokeEffect:SetControlPoint( 1, vOrigin )
            pSmokeEffect:SetControlPointOrientation( 0, Vector( 1, 0, 0 ), Vector( 0, -1, 0 ), Vector( 0, 0, 1 ) )
        end

        if self:GetSmokeEffectTickBegin() > 0 then
            local nSkipFrames = engine.TickCount() - self:GetSmokeEffectTickBegin()
            if nSkipFrames > 4 and pSmokeEffect then
                --Note: pSmokeEffect->Simulate( flSkipSeconds ) would be ideal, but it doesn't work well for long intervals. SkipToTime would be even better but it will extinguish the particle effect if it skips past 2 seconds due to some perf heuristic, and it's not clear if it skips correctly either.
                -- this doesn't happen often, and when it does, it's on connection or on replay begin/end, so a little hitch shouldn't be a problem.
                for i = 2, nSkipFrames, 2 do
                    --pSmokeEffect:Render()
                    --pSmokeEffect->Simulate( gpGlobals->interval_per_tick * 2 )
                end
            end
        end
    end
end

function ENT:OnRemove()
    if self.m_smokeParticleEffect:IsValid() then
        --self.m_smokeParticleEffect:StopEmission(nil, true)
        --self.m_smokeParticleEffect:StopEmission()
    end
end

function ENT:OnDataChanged(name, old, new)
    if ( name == "SmokeEffectTickBegin" and new > 0 --[[or self:GetDidSmokeEffect()]] ) and not self.m_bSmokeEffectSpawned then
        self:SpawnSmokeEffect()
        -- And the smoke grenade particle began! - every call but the first is extraneous here
        -- AddSmokeGrenadeHandle( this )
    end
end

-- Implement this so we never call the base class,
-- but this should never be called either.
function ENT:Detonate()
    assert(false, "Smoke grenade handles its own detonation\n")
end

function ENT:Think()
    -- HACK fix for angular velocity being reset on bouncing
    local angularVel = self:GetLocalAngularVelocity()
    if angularVel:IsZero() then
        self:SetLocalAngularVelocity(self:GetFinalAngularVelocity())
    end

    self:SetMoveType(self:GetNWMoveType())

    self:SetPos(self:Get_Pos())
    if SERVER and not self:IsInWorld() then
        self:Remove()
        return
    end

    self:NextThink( 0.2 )

    if self:GetFinalVelocity():Length() > 0.1 then
        -- Still moving. Don't detonate yet.
        return true
    end

    self:SmokeDetonate()

    return true
end

function ENT:SmokeDetonate()
    if CLIENT then return end

    self:SetSmokeEffectTickBegin( engine.TickCount() )

    -- nade has exploded, tell everyone!
    hook.Run("SWCSSmokeGrenadeDetonated", self)

    -- the old way to signal the start of smoke effect; the new way is to set the particle start tick, so that we can replay and fix the bug when we lose the smoke effect when we connect right after smoke grenade went off
    self:SetDidSmokeEffect( true)

    self:EmitSound( "BaseSmokeEffect.Sound" )

    self:SetRenderMode(RENDERMODE_TRANSCOLOR)

    self:SetMoveType(MOVETYPE_NONE)
    self:NextThink(CurTime() + 12.5)
    self.Think = self.Think_Fade

    self:SetSolid(SOLID_NONE)
end

function ENT:Think_Fade()
    self:NextThink( CurTime() )

    --local a = self:GetAlpha()
    --a = a - 1
    --self:SetAlpha( a )

    --if a == 0 then
        if SERVER then SafeRemoveEntityDelayed(self, 1.0) end
        self.m_bSelfRemove = true
        self:NextThink(CurTime() + 1.0 + engine.TickInterval())
    --end

    return true
end

function ENT:BounceSound()
    if not self:GetDidSmokeEffect() then
        self:EmitSound("SmokeGrenade_CSGO.Bounce")
    end
end

function ENT:OnBounced()
    if ( self.m_flLastBounce >= ( CurTime() - (3 * engine.TickInterval()) ) ) then
        return end

    self.m_flLastBounce = CurTime()

    -- if the smoke grenade is above ground, trace down to the ground and see where it would end up?
    local posDropSmoke = self:GetPos()
    local trSmokeTrace = {}

    util.TraceLine({
        start = posDropSmoke,
        endpos = posDropSmoke - Vector(0, 0, SmokeGrenadeRadius),
        mask = bit.band(MASK_PLAYERSOLID, bit.bnot(CONTENTS_PLAYERCLIP)),
        filter = {self, self:GetOwner(), self:GetOwner():IsValid() and unpack(self:GetOwner():GetChildren())},
        collisiongroup = COLLISION_GROUP_PROJECTILE,

        output = trSmokeTrace
    })

    if ( not trSmokeTrace.StartSolid ) then
        --if ( trSmokeTrace.Fraction >= 1.0 ) then
        --    return end -- this smoke cannot drop enough to cause extinguish

        if ( trSmokeTrace.Fraction > 0.001 ) then
            posDropSmoke = trSmokeTrace.HitPos
        end
    end

    -- See if it touches any inferno?
    local tEnts = ents.FindInSphere(self:GetPos(), 512)

    for k,v in ipairs(tEnts) do
        if v == self then continue end

        if v:GetClass() == "swcs_inferno" and v:BShouldExtinguishSmokeGrenadeBounce(self, posDropSmoke) then
            if posDropSmoke ~= self:GetPos() then
                local qAngOriginZero = Angle(0, 0, 0)
                local vVelocity = Vector(0, 0, 0)

                self:SetPos(posDropSmoke)
                self:SetAngles(qAngOriginZero)
                self:SetVelocity(vVelocity)
                self:SetLocalAngularVelocity(vVelocity)
                self:SetFinalVelocity(vVelocity)
                self:SetFinalAngularVelocity(vVelocity)
            end

            self:SmokeDetonate()
            break
        end
    end

    --local maxEnts = 64
    --CBaseEntity *list[ maxEnts ]
    --int count = UTIL_EntitiesInSphere( list, maxEnts, GetAbsOrigin(), 512, FL_ONFIRE )
    --for( int i=0 i<count ++i )
    --{
    --    if (list[i] == NULL || list[i] == this)
    --        continue
    --
    --    CInferno* pInferno = dynamic_cast<CInferno*>( list[i] )
    --
    --    if ( pInferno && pInferno->BShouldExtinguishSmokeGrenadeBounce( this, posDropSmoke ) )
    --    {
    --        if ( posDropSmoke != GetAbsOrigin() )
    --        {
    --            const QAngle qAngOriginZero = vec3_angle
    --            const Vector vVelocityZero = vec3_origin
    --            Teleport( &posDropSmoke, &qAngOriginZero, &vVelocityZero )
    --        }
    --
    --        SmokeDetonate()
    --        break
    --    }
    --}
end
