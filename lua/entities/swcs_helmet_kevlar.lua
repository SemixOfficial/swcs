AddCSLuaFile()
ENT.Base = "swcs_kevlar"
DEFINE_BASECLASS(ENT.Base)
ENT.Spawnable = true
ENT.Category = "CS:GO"
ENT.Model = Model("models/weapons/csgo/w_eq_armor_helmet.mdl")

function ENT:Use(actor,caller)
    if not (actor:IsValid() and actor:IsPlayer()) then
        return end

    if actor:Armor() < 100 then
        actor:SetArmor(100)
        self:Remove()
    end
    if not actor:HasHelmet() then
        actor:GiveHelmet()
        self:Remove()
    end
end

