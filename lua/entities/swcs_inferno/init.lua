include("shared.lua")

local TAG = "swcs_inferno"

ENT.m_NetworkTimer = swcs.CountdownTimer()
-- called every :Think() while the inferno is burning
function ENT:NetworkAllFire()
    if not self.m_NetworkTimer:IsElapsed() then
        return end

    self.m_NetworkTimer:Start(0.25)

    net.Start(TAG, false)

    net.WriteEntity(self)

    net.WriteUInt(self:GetFireCount(), 6) -- expected amount

    for i = 0, self:GetFireCount() - 1 do
        local fire = self.m_fire[i]

        net.WriteBool(fire and true or false)
        if not fire then continue end

        net.WriteFloat(self.m_fireXDelta[i])
        net.WriteFloat(self.m_fireYDelta[i])
        net.WriteFloat(self.m_fireZDelta[i])
        net.WriteBool(self.m_bFireIsBurning[i])
        net.WriteNormal(self.m_BurnNormal[i])
    end

    net.SendPVS(self:GetPos())
end
