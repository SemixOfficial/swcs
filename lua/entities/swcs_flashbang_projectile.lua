AddCSLuaFile()

ENT.Base = "baseswcsgrenade_projectile"
ENT.m_flTimeToDetonate = 1.5
ENT.IsSWCSGrenade = true

DEFINE_BASECLASS(ENT.Base)

local GRENADE_MODEL = "models/weapons/csgo/w_eq_flashbang_thrown.mdl"

function ENT:Create(pos, angs, vel, angvel, owner)
    self:SetPos(pos)
    self:SetAngles(angs)

    self:SetVelocity(vel)
    self:SetInitialVelocity(vel)
    self:SetThrower(owner)
    self:SetOwner(owner)

    self:SetLocalAngularVelocity(angvel)
    self:SetFinalAngularVelocity(angvel)
    self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)

    return self
end

function ENT:Initialize()
    self:SetModel(GRENADE_MODEL)

    self:SetDetonateTimerLength( self.m_flTimeToDetonate )

    BaseClass.Initialize(self)
end

local function PercentageOfFlashForPlayer(ply, flashPos, pevInflictor)
    if not ply:IsPlayer() then
        -- if this entity isn't a player, it's a hostage or some other entity, then don't bother with the expensive checks
        -- that come below.
        return 0.0
    end

    local FLASH_FRACTION = 0.167
    local SIDE_OFFSET = 75.0

    local pos = ply:EyePos()
    local vecRight, vecUp = Vector(),Vector()

    local tempAngle = (ply:EyePos() - flashPos):Angle()
    vecRight, vecUp = tempAngle:Right(), tempAngle:Up()

    vecRight:Normalize()
    vecUp:Normalize()

    -- Set up all the ray stuff.
    -- We don't want to let other players block the flash bang so we use this custom filter.
    local tr = {}
    local function traceFilter(ent)
        -- csgo has a check for animations on models, and whether or not the animation has a flag
        -- gmod sadly cannot replicate this behavior :(

        -- CTraceFilterNoPlayers(AndFlashbangPassableAnims)
        -- Weapons don't block flashbangs
        local bWeapon = ent:IsWeapon()
        local bGrenade = ent.IsSWCSGrenade

        if bWeapon or bGrenade then
            return false end

        -- TraceFilterNoPlayers
        if ent:IsPlayer() then
            return false end

        return true
    end
    -- CTraceFilterNoPlayersAndFlashbangPassableAnims traceFilter( pevInflictor, COLLISION_GROUP_NONE )
    local FLASH_MASK = bit.bor(MASK_OPAQUE_AND_NPCS, CONTENTS_DEBRIS)

    -- According to comment in IsNoDrawBrush in cmodel.cpp, CONTENTS_OPAQUE is ONLY used for block light surfaces,
    -- and we want flashbang traces to pass through those, since the block light surface is only used for blocking
    -- lightmap light rays during map compilation.
    FLASH_MASK = bit.band(FLASH_MASK, bit.bnot(CONTENTS_OPAQUE))

    util.TraceLine({
        start = flashPos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })

    if ((tr.Fraction == 1.0) or (tr.Entity == ply)) then
        return 1.0
    end

    local retval = 0.0

    -- check the point straight up.
    pos = flashPos + vecUp * 50.0
    util.TraceLine({
        start = flashPos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })
    -- Now shoot it to the player's eye.
    pos = ply:EyePos()
    util.TraceLine({
        start = tr.endpos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })

    if ((tr.Fraction == 1.0) or (tr.Entity == ply)) then
        retval = retval + FLASH_FRACTION
    end

    -- check the point up and right.
    pos = flashPos + vecRight * SIDE_OFFSET + vecUp * 10.0
    util.TraceLine({
        start = flashPos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })
    -- Now shoot it to the player's eye.
    pos = ply:EyePos()
    util.TraceLine({
        start = tr.endpos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })

    if ((tr.Fraction == 1.0) or (tr.Entity == ply)) then
        retval = retval + FLASH_FRACTION
    end

    -- Check the point up and left.
    pos = flashPos - vecRight * SIDE_OFFSET + vecUp * 10.0
    util.TraceLine({
        start = flashPos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })
    -- Now shoot it to the player's eye.
    pos = ply:EyePos()
    util.TraceLine({
        start = tr.endpos,
        endpos = pos,
        mask = FLASH_MASK,
        filter = traceFilter,
        output = tr,
    })

    if ((tr.Fraction == 1.0) or (tr.Entity == ply)) then
        retval = retval + FLASH_FRACTION
    end

    return retval
end

local function RadiusFlash(vecSrc, hInflictor, hAttacker, flDamage, iClassIgnore, bitsDamageType)
    vecSrc.z = vecSrc.z + 1 -- in case grenade is lying on the ground

    if not hAttacker:IsValid() then
        hattacker = hInflictor
    end

    local flRadius = 3000
    local falloff = flDamage / flRadius

    local flAdjustedDamage = 0
    local flDot = 0
    local vecEyePos = Vector()
    local vecLOS = Vector()
    local vForward = Vector()

    for _, pEntity in ipairs(ents.FindInSphere(vecSrc, flRadius)) do
        local bPlayer = pEntity:IsPlayer()

        if not bPlayer then
            continue end

        vecEyePos:Set(pEntity:EyePos())

        local percentageOfFlash = PercentageOfFlashForPlayer(pEntity, vecSrc, hInflictor)
        if percentageOfFlash > 0 then
            flAdjustedDamage = flDamage - ( vecSrc - pEntity:EyePos() ):Length() * falloff

            if flAdjustedDamage > 0 then
                vForward:Set(pEntity:EyeAngles():Forward())
                vecLOS:Set(vecSrc)
                vecLOS:Sub(vecEyePos)

                local flDistance = vecLOS:Length()

                -- Normalize both vectors so the dotproduct is in the range -1.0 <= x <= 1.0 
                vecLOS:Normalize()

                flDot = vecLOS:Dot(vForward)

                local startingAlpha = 255

                -- if target is facing the bomb, the effect lasts longer
                if ( flDot >= 0.6 ) then
                    -- looking at the flashbang
                    fadeTime = flAdjustedDamage * 2.5
                    fadeHold = flAdjustedDamage * 1.25
                elseif ( flDot >= 0.3 ) then
                    -- looking to the side
                    fadeTime = flAdjustedDamage * 1.75
                    fadeHold = flAdjustedDamage * 0.8
                elseif ( flDot >= -0.2 ) then
                    -- looking to the side
                    fadeTime = flAdjustedDamage * 1.00
                    fadeHold = flAdjustedDamage * 0.5
                else
                    -- facing away
                    fadeTime = flAdjustedDamage * 0.5
                    fadeHold = flAdjustedDamage * 0.25
                    -- //    startingAlpha = 200;
                end

                fadeTime = fadeTime * percentageOfFlash
                fadeHold = fadeHold * percentageOfFlash

                pEntity:Blind( fadeHold, fadeTime, startingAlpha )
                pEntity:Deafen( flDistance )
            end
        end
    end
end

local sv_flashbang_strength = CreateConVar( "sv_flashbang_strength", "3.55", FCVAR_REPLICATED, "Flashbang strength" )
function ENT:Detonate()
    if SERVER then
        RadiusFlash ( self:GetPos(), self, self:GetOwner(), sv_flashbang_strength:GetInt(), CLASS_NONE, DMG_BLAST) --, m_numOpponentsHit, &m_numTeammatesHit )
    end
    if CLIENT then
        local light = DynamicLight(self:EntIndex())
        light.pos = self:GetPos()
        light.r = 255
        light.g = 255
        light.b = 255
        light.brightness = 2.0
        light.size = 400
        light.dietime = CurTime() + 0.1
        light.decay = 768
    end

    if SERVER or (CLIENT and IsFirstTimePredicted()) then
        self:EmitSound("Flashbang.Explode")
        local vecSpot = self:GetPos() + Vector (0, 0, 2)
        util.Decal("Scorch", vecSpot, vecSpot + Vector(0, 0, -64), self)
    end

    -- Because we don't chain to base, tell ogs to record this detonation here
    --RecordDetonation()

    if SERVER then SafeRemoveEntity(self) end
end

function ENT:BounceSound()
    self:EmitSound("Flashbang.Bounce")
end

function ENT:AcceptInput(strInput, actor, caller, data)
    if string.lower(strInput) == "settimer" then
        self.m_flTimeToDetonate = tonumber(data)
        self:SetDetonateTimerLength(self.m_flTimeToDetonate)
    end
end
