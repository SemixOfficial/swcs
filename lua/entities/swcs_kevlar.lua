AddCSLuaFile()
ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.Spawnable = true
ENT.Category = "CS:GO"
ENT.Model = Model("models/weapons/csgo/w_eq_armor.mdl")

function ENT:Initialize()
    self:SetModel(self.Model)

    if SERVER then
        self:PhysicsInit(SOLID_VPHYSICS)

        local phys = self:GetPhysicsObject()

        if not phys:IsValid() then
            local mins, maxs = self:GetModelBounds()
            self:PhysicsInitBox(mins, maxs)
        end

        self:SetUseType(SIMPLE_USE)
    end
end

function ENT:Use(actor,caller)
    if not (actor:IsValid() and actor:IsPlayer()) then
        return end

    if actor:Armor() < 100 then
        actor:SetArmor(100)
        self:Remove()
    end
end
