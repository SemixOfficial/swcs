local swcs = {}
_G.swcs = swcs

AddCSLuaFile("swcs/cl_rendertarget.lua")
AddCSLuaFile("swcs/cl_tab.lua")
AddCSLuaFile("swcs/cl_killicon.lua")
AddCSLuaFile("swcs/cl_matproxy.lua")
AddCSLuaFile("swcs/cl_hud.lua")

include("swcs/sh_util.lua")

include("swcs/classes/sh_timer.lua")
include("swcs/classes/sh_random.lua")
include("swcs/classes/sh_ironsightcontroller.lua")

include("swcs/sh_cvars.lua")
include("swcs/sh_player.lua")

include("swcs/sh_knives.lua")

include("swcs/sh_grenades.lua")

include("swcs/sh_effects.lua")

if CLIENT then
	include("swcs/cl_rendertarget.lua")
	include("swcs/cl_tab.lua")
	include("swcs/cl_killicon.lua")
	include("swcs/cl_matproxy.lua")
	include("swcs/cl_hud.lua")
end

-- look into CEconItemView::CreateCustomWeaponMaterials()
local genned_mats = {}
function swcs.GenerateEconTexture(params)
	if SERVER then return end

	local basetexture = params.basetexture
	local flWearValue = params.wearvalue
	local normalmap   = params.normal

	local filename = string.GetFileFromFilename(basetexture)
	if not filename then error("bad texture path", 2) end

	local hash = util.CRC(Format("%s_%f", filename, flWearValue))
	local mat_name = Format("swcs_%s_%x", filename, hash)

	local mat = genned_mats[mat_name]
	if not mat or mat:IsError() then
		mat = CreateMaterial(mat_name, "VertexLitGeneric", {
			["$basetexture"] = basetexture,
			["$bumpmap"] = normalmap
		})
	else
		mat:SetTexture("$basetexture", basetexture)

		if normalmap then
			mat:SetTexture("$bumpmap", normalmap)
		end

		local tex = mat:GetTexture("$basetexture")
		if tex then
			tex:Download()
		end
	end

	genned_mats[mat_name] = mat

	return mat_name, mat
end

if CLIENT then
	local swcs_hands = CreateClientConVar("swcs_hands", "0", nil, nil, "set ur csgo hands lol")
	local swcs_hands_skin = CreateClientConVar("swcs_hands_skin", "0", nil, nil, "set ur hand skin tone")
	local hands_table = {
		Model "models/weapons/v_models/csgo/arms/bare/v_bare_hands.mdl",
		Model "models/weapons/v_models/csgo/arms/anarchist/v_glove_anarchist.mdl",
		Model "models/weapons/v_models/csgo/arms/ghost/v_ghost_hands.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_bloodhound/v_glove_bloodhound.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_bloodhound/v_glove_bloodhound_brokenfang.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_bloodhound/v_glove_bloodhound_hydra.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_fingerless/v_glove_fingerless.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_fullfinger/v_glove_fullfinger.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_handwrap_leathery/v_glove_handwrap_leathery.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_hardknuckle/v_glove_hardknuckle.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_hardknuckle/v_glove_hardknuckle_black.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_hardknuckle/v_glove_hardknuckle_blue.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_motorcycle/v_glove_motorcycle.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_slick/v_glove_slick.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_specialist/v_glove_specialist.mdl",
		Model "models/weapons/v_models/csgo/arms/glove_sporty/v_glove_sporty.mdl",
	}

	local swcs_sleeves = CreateClientConVar("swcs_sleeves", "0", nil, nil, "set ur csgo sleeves lol (requires swcs_hands > 0)")
	local sleeves_table = {
		Model "models/weapons/v_models/csgo/arms/anarchist/v_sleeve_anarchist.mdl",

		-- Balkan
		Model "models/weapons/v_models/csgo/arms/balkan/v_sleeve_balkan.mdl", -- old balkan
		Model "models/weapons/v_models/csgo/arms/balkan/v_sleeve_balkan_v2_variantf.mdl", -- new balkan (variant 1)
		Model "models/weapons/v_models/csgo/arms/balkan/v_sleeve_balkan_v2_variantg.mdl", -- new balkan (variant 2)
		Model "models/weapons/v_models/csgo/arms/balkan/v_sleeve_balkan_v2_varianth.mdl", -- new balkan (variant 3)
		Model "models/weapons/v_models/csgo/arms/balkan/v_sleeve_balkan_v2_variantj.mdl", -- new balkan (variant 4)

		-- Heavies
		Model "models/weapons/v_models/csgo/arms/ctm_heavy/v_sleeve_ctm_heavy.mdl",
		Model "models/weapons/v_models/csgo/arms/phoenix_heavy/v_sleeve_phoenix_heavy.mdl",

		-- FBI
		Model "models/weapons/v_models/csgo/arms/fbi/v_sleeve_fbi.mdl",
		Model "models/weapons/v_models/csgo/arms/fbi/v_sleeve_fbi_dark.mdl",
		Model "models/weapons/v_models/csgo/arms/fbi/v_sleeve_fbi_green.mdl",
		Model "models/weapons/v_models/csgo/arms/fbi/v_sleeve_fbi_light_green.mdl",

		-- hello ct
		Model "models/weapons/v_models/csgo/arms/gign/v_sleeve_gign.mdl",
		Model "models/weapons/v_models/csgo/arms/gsg9/v_sleeve_gsg9.mdl",
		Model "models/weapons/v_models/csgo/arms/idf/v_sleeve_idf.mdl",

		-- DZ sleeve
		Model "models/weapons/v_models/csgo/arms/jumpsuit/v_sleeve_jumpsuit.mdl",

		-- THE ROLEX
		Model "models/weapons/v_models/csgo/arms/pirate/v_pirate_watch.mdl",

		-- "Professional"
		Model "models/weapons/v_models/csgo/arms/professional/v_sleeve_professional.mdl",

		-- SAS
		Model "models/weapons/v_models/csgo/arms/sas/v_sleeve_sas.mdl",
		Model "models/weapons/v_models/csgo/arms/sas/v_sleeve_sas_ukmtp.mdl",

		-- Separatist sleeve
		Model "models/weapons/v_models/csgo/arms/separatist/v_sleeve_separatist.mdl",

		-- Seal Team 6
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_flektarn.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_green.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_st6.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_st6_v2_variante.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_st6_v2_variantg.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_st6_v2_variantk.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_st6_v2_variantm.mdl",
		Model "models/weapons/v_models/csgo/arms/st6/v_sleeve_usaf.mdl",

		-- SWAT Team
		Model "models/weapons/v_models/csgo/arms/swat/v_sleeve_swat.mdl",
		Model "models/weapons/v_models/csgo/arms/swat/v_sleeve_swat_blue.mdl",
		Model "models/weapons/v_models/csgo/arms/swat/v_sleeve_swat_green.mdl",

		-- i just got out the hospital
		Model "models/weapons/v_models/csgo/arms/wristband/v_sleeve_wristband.mdl",
	}

	local csgo_hands = NULL
	local csgo_sleeves = NULL

	hook.Add("PreDrawViewModel", "swcs.hands", function(vm, ply, wep)
		if wep.IsSWCSWeapon then
			wep.UseHands = swcs_hands:GetInt() == 0

			if swcs_hands:GetInt() > 0 then
				if not IsValid(csgo_hands) then
					csgo_hands = ClientsideModel(hands_table[swcs_hands:GetInt()])
					csgo_hands:SetNoDraw(true)
					csgo_hands:SetParent(vm)
					csgo_hands:AddEffects(EF_BONEMERGE)
					csgo_hands:AddEffects(EF_BONEMERGE_FASTCULL)
				end
				if csgo_hands:GetModel() ~= hands_table[swcs_hands:GetInt()] then
					csgo_hands:SetModel(hands_table[swcs_hands:GetInt()])
				end
				if csgo_hands:GetParent() ~= vm then
					csgo_hands:SetParent(vm)
				end
				if csgo_hands:GetSkin() ~= swcs_hands_skin:GetInt() then
					csgo_hands:SetSkin(swcs_hands_skin:GetInt())
				end

				if swcs_sleeves:GetInt() > 0 then
					if not IsValid(csgo_sleeves) then
						csgo_sleeves = ClientsideModel(sleeves_table[swcs_sleeves:GetInt()])
						csgo_sleeves:SetNoDraw(true)
						csgo_sleeves:SetParent(vm)
						csgo_sleeves:AddEffects(EF_BONEMERGE)
						csgo_sleeves:AddEffects(EF_BONEMERGE_FASTCULL)
					end
					if csgo_sleeves:GetModel() ~= sleeves_table[swcs_sleeves:GetInt()] then
						csgo_sleeves:SetModel(sleeves_table[swcs_sleeves:GetInt()])
					end
					if csgo_sleeves:GetParent() ~= vm then
						csgo_sleeves:SetParent(vm)
					end

					csgo_sleeves:DrawModel()
				end

				csgo_hands:DrawModel()
			end
		end
	end)
end

--local PLAYER_FALL_PUNCH_THRESHOLD = 350
local PLAYER_FATAL_FALL_SPEED = 922.5 -- hl2 == 922.5f, csgo == 1024
local function CheckFalling(ply, wep)
	-- this function really deals with landing, not falling, so ignore everything else
	if ply:GetGroundEntity() == NULL or ply.m_flFallVelocity < 0 then
		return end

	local flFallVel = ply.m_flFallVelocity
	if flFallVel > 16.0 and flFallVel <= PLAYER_FATAL_FALL_SPEED then
		-- punch view when we hit the ground
		local punchAngle = wep:GetUninterpolatedViewPunchAngle()
		punchAngle.x = (flFallVel * 0.001)

		if ( punchAngle.x < 0.75 ) then
			punchAngle.x = 0.75
		end

		wep:SetViewPunchAngle( punchAngle )
	end

	if wep.OnLand then
		wep:OnLand(flFallVel)
	end

	ply.m_flFallVelocity = 0
end

hook.Add("Initialize", "swcs.ammo",function()
	game.AddAmmoType({name = "swcs_flashbang", maxcarry = 2})
	game.AddAmmoType({name = "swcs_hegrenade", maxcarry = 1})
	game.AddAmmoType({name = "swcs_smokegrenade", maxcarry = 1})
	game.AddAmmoType({name = "swcs_firegrenade", maxcarry = 1})
end)

hook.Add("SetupMove", "swcs.movement", function(ply, move, cmd)
	if not ply:IsValid() then return end

	if not ply.m_flFallVelocity then
		ply.m_flFallVelocity = 0
	end

	local wep = ply:GetActiveWeapon()

	-- done to fix gmod's shoot pos being behind 1 tick on client
	ply.m_vSavedShootPos = ply:GetShootPos()

	if ply:GetNWBool("m_bIsDefusing", false) then
		move:SetMaxClientSpeed(1)
		move:SetMaxSpeed(1)
	end

	if IsValid(wep) then
		if SERVER and ply.swcs_canzoom == nil then
			ply.swcs_canzoom = ply:GetCanZoom()
		end

		if wep.IsSWCSWeapon then
			local mult = wep:GetMaxSpeed() / 250
			if mult < 0 then
				mult = 1
			end

			if cmd:KeyDown(IN_ATTACK) and wep:GetShotsFired() >= 1 then
				mult = mult * wep:GetAttackMovespeedFactor()
			end

			move:SetMaxClientSpeed(move:GetMaxClientSpeed() * mult)
			move:SetMaxSpeed(move:GetMaxSpeed() * mult)

			if wep:GetMaxSpeed() == 0 then
				move:SetMaxClientSpeed(1)
				move:SetMaxSpeed(1)
			end

			CheckFalling(ply, wep)
			ply.m_flFallVelocity = -move:GetVelocity().z

			if wep.OnMove then
				wep:OnMove(ply, move, cmd)
			end

			if SERVER then
				ply:SetCanZoom(false)
			end
		else
			if SERVER and ply.swcs_canzoom then
				ply:SetCanZoom(ply.swcs_canzoom)
				ply.swcs_canzoom = nil
			end
		end
	end
end)

local function ScaleDamage(victim, hitgroup, dmg)
	local atk = NULL
	local wep = NULL

	if dmg:GetAttacker():IsValid() and dmg:GetAttacker():IsPlayer() then
		atk = dmg:GetAttacker()
	end

	if dmg:GetInflictor():IsValid() and dmg:GetInflictor():IsWeapon() then
		wep = dmg:GetInflictor()
	end

	if atk:IsValid() and not wep:IsValid() and atk:GetActiveWeapon():IsValid() then
		wep = atk:GetActiveWeapon()
	end

	if wep:IsValid() and wep.IsSWCSWeapon then
		wep:ApplyDamageScale(dmg, {HitGroup = hitgroup}, dmg:GetDamage())

		-- client doesnt deal damage, and we return true so that we can hide the engine blood spatter effect
		-- return false so that the gamemode func doesnt get called
		-- TODO: add toggle for this??
		return CLIENT
	end
end
hook.Add("ScalePlayerDamage", "swcs.dmg", ScaleDamage)
hook.Add("ScaleNPCDamage", "swcs.dmg", ScaleDamage)

hook.Add("UpdateAnimation", "swcs.ply_anim", function(ply)
	local wep = ply:GetActiveWeapon()
	if wep.IsSWCSWeapon then
		local flEyePitch = ply:GetPoseParameter("aim_pitch")
		if CLIENT then
			local iPoseParam = ply:LookupPoseParameter("aim_pitch")

			if iPoseParam ~= -1 then
				local flMin, flMax = ply:GetPoseParameterRange(iPoseParam)
				flEyePitch = math.Remap(flEyePitch, 0,1, flMin, flMax)
			end
		end

		flEyePitch = math.NormalizeAngle(flEyePitch + wep:GetNWFloat("m_flThirdpersonRecoil", 0))
		ply:SetPoseParameter("aim_pitch", flEyePitch)
	end
end)

-- map transition SWEP:Initialize()
if SERVER then
	hook.Add("PlayerSpawn", "swcs.map_transition", function(ply, t)
		if not t then return end

		for _, w in ipairs(ply:GetWeapons()) do
			if w.IsSWCSWeapon then
				IronSightController(w.m_IronSightController)
				w:Initialize(false, true)
			end
		end
	end)
end

-- i wrote this while intoxicated oops
local to_disable = {
	["weapon_zm_improvised"] = true,
	["weapon_zm_mac10"] = true,
	["weapon_zm_pistol"] = true,
	["weapon_zm_revolver"] = true,
	["weapon_zm_rifle"] = true,
	["weapon_zm_shotgun"] = true,
	["weapon_zm_sledge"] = true,
	["weapon_ttt_glock"] = true,
	["weapon_ttt_m16"] = true
}
hook.Add("PreGamemodeLoaded", "swcs.ttt_init", function()
	local bIsTTT = util.NetworkStringToID("TTT_RoundState") ~= 0

	if not bIsTTT then return end

	local spawnable_weps = SERVER and ents.TTT.GetSpawnableSWEPs() or {}

	-- todo: add toggle to disable this?? prolly lol
	table.Empty(spawnable_weps)

	-- add our weps to auto spawn :)
	for i, t in ipairs(weapons.GetList()) do
		if weapons.IsBasedOn(t.ClassName, "weapon_swcs_base") and t.Spawnable and not t.AdminSpawnable and t.AutoSpawnable == nil then
			if t.InLoadoutFor then continue end

			Msg("[swcs] ") print(Format("made %s for ttt", t.ClassName))
			t.AutoSpawnable = true
			table.insert(spawnable_weps, t)

			--local ItemVisuals = util.KeyValuesToTable(t.ItemDefVisuals, true, false)
			local ItemAttributes = util.KeyValuesToTable(t.ItemDefAttributes, true, false)
			local weapon_type = string.lower(ItemAttributes["weapon_type"])

			local max_prim = tonumber(ItemAttributes["primary reserve ammo max"])
			t.Primary.ClipMax = max_prim

			if weapon_type == "pistol" then
				t.Kind = WEAPON_PISTOL
				t.Slot = 1

				if t.ClassName == "weapon_swcs_deagle" or t.ClassName == "weapon_swcs_revolver" then
					t.Primary.Ammo = "AlyxGun"
					t.AmmoEnt = "item_ammo_revolver_ttt"
				else
					t.Primary.Ammo = "pistol"
					t.AmmoEnt = "item_ammo_pistol_ttt"
				end
			elseif t.Base ~= "weapon_swcs_knife" then
				t.Kind = WEAPON_HEAVY
				t.Slot = 2

				if weapon_type == "shotgun" then
					t.Primary.Ammo = "Buckshot"
					t.AmmoEnt = "item_box_buckshot_ttt"
				elseif weapon_type == "sniperrifle" then
					t.Primary.Ammo = "357"
					t.AmmoEnt = "item_ammo_357_ttt"
				else
					t.Primary.Ammo = "smg1"
					t.AmmoEnt = "item_ammo_smg1_ttt"
				end
			end
		elseif to_disable[t.ClassName] then
			Msg("[swcs] ") print("obliterated ttt wep", t.ClassName)
			t.AutoSpawnable = false
		end
	end

	if CLIENT then return end

	-- overwrite default ttt weapons with our own
	timer.Simple(0, function()
		ents.TTT._ReplaceEntities = ents.TTT._ReplaceEntities or ents.TTT.ReplaceEntities

		local _, ReplaceAmmo = debug.getupvalue(ents.TTT._ReplaceEntities, 1)
		local _, ReplaceWeapons = debug.getupvalue(ents.TTT._ReplaceEntities, 2)
		local _, RemoveCrowbars = debug.getupvalue(ents.TTT._ReplaceEntities, 3)

		local _, _ReplaceWeaponSingle = debug.getupvalue(ReplaceWeapons, 1)
		local _, ReplaceSingle = debug.getupvalue(_ReplaceWeaponSingle, 2)

		local swcs_all_weps = {}

		for k, v in next, weapons.GetList() do
			if weapons.IsBasedOn(v.ClassName, "weapon_swcs_base") and not v.IsBaseWep then
				local keyvals = util.KeyValuesToTable(v.ItemDefVisuals)
				local strWeaponType = string.lower(keyvals.weapon_type)

				if not swcs_all_weps[strWeaponType] then
					swcs_all_weps[strWeaponType] = {}
				end

				if v.ClassName ~= "weapon_swcs_deagle" and v.ClassName ~= "weapon_swcs_revolver" then
					table.insert(swcs_all_weps[strWeaponType], v.ClassName)
				end
			end
		end

		local deagles = {
			"weapon_swcs_deagle",
			"weapon_swcs_revolver"
		}
		local ttt_weapon_replace = {
			["weapon_zm_mac10"] = function(ent)
				--print("REPLACE PLS", ent)
				return table.Random(swcs_all_weps.submachinegun)
				-- random smg
			end,
			["weapon_zm_shotgun"] = function(ent)
				--print("REPLACE PLS", ent)
				return table.Random(swcs_all_weps.shotgun)
				-- random shotgun
			end,
			["weapon_ttt_m16"] = function(ent)
				--print("REPLACE PLS", ent)
				return table.Random(swcs_all_weps.rifle)
				-- random assault rifle
			end,
			["weapon_zm_rifle"] = function(ent)
				--print("REPLACE PLS", ent)
				return table.Random(swcs_all_weps.sniperrifle)
				-- random sniper
			end,
			["weapon_zm_pistol"] = function(ent)
				return table.Random(swcs_all_weps.pistol)
				--print("REPLACE PISTOL PLS", ent)
				-- random pistol
			end,
			["weapon_zm_sledge"] = function(ent)
				--
				--print("REPLACE PLS", ent)
				return table.Random(swcs_all_weps.machinegun)
				-- random LMG
			end,
			["weapon_zm_revolver"] = function(ent)
				return table.Random(deagles)
			end,

			--["item_ammo_pistol_ttt"] = "item_ammo_pistol_ttt",
			--["weapon_zm_molotov"] = "weapon_zm_molotov"
		}

		local function ReplaceWeaponSingleSWCS(ent, cls)
			-- Loadout weapons immune
			-- we use a SWEP-set property because at this state all SWEPs identify as weapon_swep
			if ent.AllowDelete == false then
				return
			else
				if cls == nil then cls = ent:GetClass() end

				local rpl = ttt_weapon_replace[cls]
				if isfunction(rpl) then
					rpl = rpl(ent)
				end

				if rpl then
					ReplaceSingle(ent, rpl)
				end
			end
		end

		local function SWCSReplaceWeapons()
			for _, ent in ipairs(ents.FindByClass("weapon_*")) do
				ReplaceWeaponSingleSWCS(ent)
			end
		end

		ents.TTT.ReplaceEntities = function()
			ReplaceAmmo()
			ReplaceWeapons()
			RemoveCrowbars()
			ents.TTT.RemoveRagdolls()

			SWCSReplaceWeapons()
		end
	end)
end)
