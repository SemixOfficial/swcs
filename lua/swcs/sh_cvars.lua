AddCSLuaFile()

SWCS_DEBUG_AE           = CreateConVar("swcs_debug_animevent", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY}, "")
SWCS_DEBUG_RECOIL       = CreateConVar("swcs_debug_recoil", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY}, "")
SWCS_DEBUG_RECOIL_DECAY = CreateConVar("swcs_debug_decay", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY}, "")
SWCS_DEBUG_PENETRATION  = CreateConVar("swcs_debug_penetration", "0", {FCVAR_REPLICATED, FCVAR_NOTIFY}, "")

SWCS_SPREAD_MAX_SEEDS = CreateConVar("swcs_weapon_max_spread_seed", "255", {FCVAR_REPLICATED, FCVAR_ARCHIVE}, "how many spread seeds csgo weapons can have")
SWCS_SPREAD_SHARE_SEED = CreateConVar("swcs_weapon_sync_seed", "1", {FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE}, "synchronize spread seeds on server and client")

SWCS_DEPLOY_OVERRIDE = CreateConVar("swcs_deploy_override", "1", {FCVAR_REPLICATED, FCVAR_ARCHIVE}, "deploy speed override multiplier")
SWCS_INDIVIDUAL_AMMO = CreateConVar("swcs_weapon_individual_ammo", "0", {FCVAR_REPLICATED, FCVAR_ARCHIVE}, "weapons store their own ammo, and don't pull from player's ammo")

if CLIENT then
	CreateConVar("cl_crosshair_recoil", "0", {FCVAR_CHEAT--[[, FCVAR_CLIENTDLL]]}, "Recoil/aimpunch will move the user's crosshair to show the effect")
end
CreateClientConVar("cl_bob_lower_amt", "21", nil, nil, "The amount the viewmodel lowers when running", 5, 30)
CreateClientConVar("cl_crosshairstyle", "4", nil, nil, "0 = DEFAULT, 1 = DEFAULT STATIC, 2 = ACCURATE SPLIT (accurate recoil/spread feedback with a fixed inner part) 3 = ACCURATE DYNAMIC (accurate recoil/spread feedback) 4 = CLASSIC STATIC, 5 = OLD CS STYLE (fake recoil - inaccurate feedback)")
CreateClientConVar("cl_crosshairdot", "1")
CreateClientConVar("cl_crosshair_t", "0", nil, nil, "T style crosshair")
CreateClientConVar("cl_crosshairthickness", "1")
CreateClientConVar("cl_crosshairsize", "5")
CreateClientConVar("cl_crosshairgap", "0")
CreateClientConVar("cl_crosshairgap_useweaponvalue", "0", nil, nil, "If set to 1, the gap will update dynamically based on which weapon is currently equipped")
CreateClientConVar("cl_crosshair_drawoutline", "1")
CreateClientConVar("cl_crosshair_outlinethickness", "1", nil, nil, "Set how thick you want your crosshair outline to draw (0.1-3)")
CreateClientConVar("cl_crosshair_dynamic_splitdist", "7", true, nil, "If using cl_crosshairstyle 2, this is the distance that the crosshair pips will split into 2. (default is 7)")
CreateClientConVar("cl_crosshair_dynamic_splitalpha_innermod", "1", true, nil, "If using cl_crosshairstyle 2, this is the alpha modification that will be used for the INNER crosshair pips once they've split. [0 - 1]")
CreateClientConVar("cl_crosshair_dynamic_splitalpha_outermod", "0.5", true, nil, "If using cl_crosshairstyle 2, this is the alpha modification that will be used for the OUTER crosshair pips once they've split. [0.3 - 1]")
CreateClientConVar("cl_crosshair_dynamic_maxdist_splitratio", "0.35", true, nil, "If using cl_crosshairstyle 2, this is the ratio used to determine how long the inner and outer xhair pips will be. [inner = cl_crosshairsize*(1-cl_crosshair_dynamic_maxdist_splitratio) outer = cl_crosshairsize*cl_crosshair_dynamic_maxdist_splitratio]  [0 - 1]")
CreateClientConVar("cl_crosshaircolor", "1")
CreateClientConVar("cl_crosshairusealpha", "0")
CreateClientConVar("cl_crosshaircolor_r", "255")
CreateClientConVar("cl_crosshaircolor_g", "0")
CreateClientConVar("cl_crosshaircolor_b", "255")
CreateClientConVar("cl_crosshairalpha", "200")
CreateClientConVar("cl_crosshair_sniper_width", "1", nil,nil, "If >1 sniper scope cross lines gain extra width (1 for single-pixel hairline)")
CreateConVar("sv_showimpacts", "0", {FCVAR_REPLICATED}, "Shows client (red) and server (blue) bullet impact point (1=both, 2=client-only, 3=server-only)")
CreateConVar("sv_showimpacts_penetration", "0", {FCVAR_REPLICATED}, "Shows extra data when bullets penetrate. (use sv_showimpacts_time to increase time shown)")
CreateConVar("sv_showimpacts_time", "4", {FCVAR_REPLICATED}, "Duration bullet impact indicators remain before disappearing")
CreateClientConVar("viewmodel_offset_x", "0.0")
CreateClientConVar("viewmodel_offset_y", "0.0")
CreateClientConVar("viewmodel_offset_z", "0.0")

-- used in calcview to follow spray pattern
CreateConVar("view_recoil_tracking", "0.45", {FCVAR_REPLICATED, FCVAR_CHEAT}, "How closely the view tracks with the aim punch from weapon recoil")

-- used for view model to follow spray pattern
CreateClientConVar("viewmodel_recoil", "1.0", nil, nil, "Amount of weapon recoil/aimpunch to display on viewmodel")

CreateClientConVar("weapon_debug_spread_show", "0", FCVAR_CHEAT, "Enables display of weapon accuracy; 1: show accuracy box, 3: show accuracy with dynamic crosshair")
CreateConVar("weapon_near_empty_sound", "1", {FCVAR_CHEAT, FCVAR_REPLICATED}, "")
CreateConVar("weapon_air_spread_scale", "1.0", {FCVAR_CHEAT, FCVAR_REPLICATED}, "Scale factor for jumping inaccuracy, set to 0 to make jumping accuracy equal to standing")
CreateConVar("weapon_recoil_decay_coefficient", "2.0", {FCVAR_CHEAT, FCVAR_REPLICATED}, "")
CreateConVar("weapon_accuracy_forcespread", "0", FCVAR_REPLICATED, "Force spread to the specified value.")
CreateConVar("weapon_accuracy_nospread", "0", FCVAR_REPLICATED, "Disable weapon inaccuracy spread")
CreateConVar("weapon_accuracy_shotgun_spread_patterns", "1", {FCVAR_REPLICATED, FCVAR_NOTIFY})
CreateConVar("weapon_recoil_cooldown", "0.55", {FCVAR_REPLICATED, FCVAR_CHEAT}, "Amount of time needed between shots before restarting recoil")
CreateConVar("weapon_recoil_scale", "2", FCVAR_REPLICATED, "Overall scale factor for recoil.")
CreateConVar("weapon_recoil_view_punch_extra", "0.055", FCVAR_REPLICATED, "Additional (non-aim) punched added to view from recoil")

cvars.AddChangeCallback("sv_showimpacts", function(name,old,new)
	local num = tonumber(new)

	if SERVER then
		for k,v in next,player.GetAll() do
			v:ConCommand("developer " .. (num > 0 and 1 or 0))
		end
	--else
	--	
	end
end, "swcs.developer_change")
