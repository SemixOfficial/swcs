AddCSLuaFile()

function swcs.Approach(target, value, speed)
    local delta = target - value

    if ( delta > speed ) then
        value = value + speed
    elseif ( delta < -speed ) then
        value = value - speed
    else
        value = target end

    return value
end

IN_ATTACK3 = bit.lshift(1, 25)

function swcs.ImpactTrace(tr, iDamageType, ply)
    if not tr.Entity or tr.HitSky then
        return end
    if tr.Fraction == 1 then
        return end
    if tr.HitNoDraw then
        return end

    local data = EffectData()
    data:SetOrigin(tr.HitPos)
    data:SetStart(tr.StartPos)
    data:SetSurfaceProp(tr.SurfaceProps)
    data:SetDamageType(iDamageType)
    data:SetHitBox(tr.HitBox)
    data:SetEntity(tr.Entity)
    if SERVER then
        data:SetEntIndex(tr.Entity:EntIndex())
    end

    if SERVER or (CLIENT and IsFirstTimePredicted()) then
        swcs.fx.ImpactEffect(data, ply)
    end
end

local FX_WATER_IN_SLIME = 0x1
function swcs.BulletImpact(tr, ply)
    -- see if the bullet ended up underwater and started out of the water
    if bit.band(util.PointContents(tr.HitPos), bit.bor(CONTENTS_WATER, CONTENTS_SLIME)) ~= 0 then
        local waterTrace = {}
        util.TraceLine({
            start = tr.StartPos,
            endpos = tr.HitPos,
            mask = bit.bor(CONTENTS_WATER,CONTENTS_SLIME),
            filter = ply,
            collisiongroup = COLLISION_GROUP_NONE,
            output = waterTrace,
        })

        if not waterTrace.AllSolid then
            local data = EffectData()
            data:SetOrigin(waterTrace.HitPos)
            data:SetNormal(waterTrace.HitNormal)
            data:SetScale(g_ursRandom:RandomFloat(8, 12))

            if bit.band(waterTrace.Contents, CONTENTS_SLIME) ~= 0 then
                data:SetFlags(bit.bor(data:GetFlags(), FX_WATER_IN_SLIME))
            else
                data:SetFlags(bit.bnot(FX_WATER_IN_SLIME))
            end

            if not game.SinglePlayer() and SERVER and IsValid(ply) and ply:IsPlayer() then
                SuppressHostEvents(ply)
            end

            if SERVER or (CLIENT and IsFirstTimePredicted()) then
                util.Effect("gunshotsplash", data, not game.SinglePlayer())
            end

            return
        end
    end

    swcs.ImpactTrace(tr, DMG_BULLET, ply)
end

local function fsel(c, x, y)
    return c >= 0 and x or y
end
function swcs.RemapClamped(val,a,b,c,d)
    if a == b then
        return fsel(val - b, d, c)
    end

    local cVal = (val - a) / (b - a)
    cVal = math.Clamp(cVal, 0, 1)
    return c + (d - c) * cVal
end

local lastAmt = -1
local lastExponent = -1
function swcs.Bias(x, biasAmt)
    if lastAmt ~= biasAmt then
        lastExponent = math.log(biasAmt) * -1.4427 -- (-1.4427 = 1 / log(0.5))
    end

    return math.pow(x, lastExponent)
end
function swcs.Gain(x, biasAmt)
    if x < 0.5 then
        return 0.5 * swcs.Bias(2 * x, 1-biasAmt)
    else
        return 1 - 0.5 * swcs.Bias(2 - 2 * x, 1-biasAmt)
    end
end

-- NOTE: not lag compensated by default
--       you must call player:LagCompensation
--       otherwise ur a retard
function swcs.FireBullets(wep, bulletInfo)
    if not (isentity(wep) and wep:IsValid() and wep.IsSWCSWeapon) then return false end

    local ent = wep:GetPlayerOwner()
    if not ent then
        return end

    local info_copy = table.Copy(bulletInfo)
    local bRet = hook.Run("EntityFireBullets", ent, info_copy)
    if bRet == true then
        bulletInfo = info_copy
    elseif bRet == false then
        return
    end

    local filter = {}
    --if g_CapsuleHitboxes then
    --    filter = g_CapsuleHitboxes:GetEntitiesWithCapsuleHitboxes(ent)
    --end

    -- don't shoot yourself, loser
    table.insert(filter, ent)

    local tr = util.TraceLine({
        start = bulletInfo.Src,
        endpos = bulletInfo.Src + (bulletInfo.Dir * bulletInfo.Distance),
        mask = CS_MASK_SHOOT,
        filter = filter,
    })

    --if g_CapsuleHitboxes then
    --    table.remove(filter) -- remove owner from filter
    --    g_CapsuleHitboxes:IntersectRayWithEntities(tr, filter)
    --end

    swcs.BulletImpact(tr, ent)

    wep:DoTracer(wep.ItemVisuals.tracer_effect, tr.StartPos, tr.HitPos)

    local flCurrentDistance = tr.StartPos:Distance(tr.HitPos)
    local fDamage = bulletInfo.Damage * math.pow(wep:GetRangeModifier(), flCurrentDistance / 500)

    local dmg = DamageInfo()
    dmg:SetAttacker(bulletInfo.Attacker)
    dmg:SetInflictor(bulletInfo.Attacker:GetActiveWeapon())
    dmg:SetBaseDamage(bulletInfo.Damage)
    dmg:SetDamage(bulletInfo.Damage)
    dmg:SetDamageType(DMG_BULLET)
    dmg:SetReportedPosition(tr.StartPos)
    dmg:SetDamagePosition(tr.HitPos)
    dmg:SetDamageForce(vector_origin)

    if isfunction(bulletInfo.Callback) then
        bulletInfo.Callback(ent, table.Copy(tr), dmg)
    end

    dmg:SetDamage(math.ceil(fDamage))
    dmg:SetBaseDamage(bulletInfo.Damage)
    dmg:SetReportedPosition(tr.StartPos)
    dmg:SetDamagePosition(tr.HitPos)
    dmg:SetDamageForce(vector_origin)

    if tr.Entity:IsValid() or tr.Entity:IsWorld() then
        -- disable player pushback on bullet damage
        -- what the fuck
        if tr.Entity:IsPlayer() then
            dmg:GetAttacker():AddSolidFlags(FSOLID_TRIGGER)
        end

        swcs.fx.TraceAttack(tr.Entity, dmg, bulletInfo.Dir, tr)
        tr.Entity:DispatchTraceAttack(dmg, tr)

        if tr.Entity:IsPlayer() then
            dmg:GetAttacker():RemoveSolidFlags(FSOLID_TRIGGER)
        end
    end

    -- bullet flinching/tagging
    -- aka movement slow down
    -- code goes here
end

function swcs.IsBreakableEntity(ent)
    if not IsValid(ent) then return false end

    -- first check to see if it's already broken
    if ent:Health() < 0 and ent:GetMaxHealth() > 0 then
        return true
    end

    -- If we won't be able to break it, don't try
    if SERVER then
        local var = ent:GetInternalVariable("m_takedamage")
        if tonumber(var) and tonumber(var) ~= 2 then
            return false
        end
    end

    if ent:GetCollisionGroup() ~= COLLISION_GROUP_PUSHAWAY and ent:GetCollisionGroup() ~= COLLISION_GROUP_BREAKABLE_GLASS and ent:GetCollisionGroup() ~= COLLISION_GROUP_NONE then
        return false
    end

    if ent:Health() > 200 then
        return false
    end

    if ent:GetClass() == "func_breakable" or ent:GetClass() == "func_breakable_surf" then
        return true
    end

    return true
end

local IsGunWeapon = {
    ["pistol"] = true,
    ["submachinegun"] = true,
    ["rifle"] = true,
    ["shotgun"] = true,
    ["sniperrifle"] = true,
    ["machinegun"] = true,
}
function swcs.IsGunWeapon(wep_type)
    return IsGunWeapon[wep_type]
end

--============================================================================================================
-- Utility functions for physics damage force calculation 
--============================================================================================================
-------------------------------------------------------------------------------
-- Purpose: Returns an impulse scale required to push an object.
-- Input  : flTargetMass - Mass of the target object, in kg
--			flDesiredSpeed - Desired speed of the target, in inches/sec.
-------------------------------------------------------------------------------
function swcs.ImpulseScale( flTargetMass, flDesiredSpeed )
    return flTargetMass * flDesiredSpeed
end

local phys_pushscale = GetConVar("phys_pushscale")

-------------------------------------------------------------------------------
-- Purpose: Fill out a takedamageinfo with a damage force for an explosive
-------------------------------------------------------------------------------
function swcs.CalculateExplosiveDamageForce( info, vecDir, vecForceOrigin, flScale )
    info:SetDamagePosition( vecForceOrigin )

    local flClampForce = swcs.ImpulseScale( 75, 400 )

    -- Calculate an impulse large enough to push a 75kg man 4 in/sec per point of damage
    local flForceScale = info:GetBaseDamage() * swcs.ImpulseScale( 75, 4 )

    if flForceScale > flClampForce then
        flForceScale = flClampForce
    end

    -- Fudge blast forces a little bit, so that each
    -- victim gets a slightly different trajectory. 
    -- This simulates features that usually vary from
    -- person-to-person variables such as bodyweight,
    -- which are all indentical for characters using the same model.
    flForceScale = flForceScale * g_ursRandom:RandomFloat( 0.85, 1.15 )

    -- Calculate the vector and stuff it into the takedamageinfo
    local vecForce = vecDir
    vecForce:Normalize()
    vecForce:Mul(flForceScale)
    vecForce:Mul(phys_pushscale:GetFloat())
    vecForce:Mul(flScale)
    info:SetDamageForce( vecForce )
end

function swcs.RadiusDamage(info, vecSrcIn, flRadius, bIgnoreWorld)
    local tr = {}
    local falloff, damagePercentage
    local vecSpot, vecToTarget, vecEndPos = Vector(), Vector(), Vector()

    local vecSrc = Vector(vecSrcIn)

    damagePercentage = 1.0

    if flRadius > 0 then
        falloff = info:GetDamage() / flRadius
    else
        falloff = 1.0
    end

    local flInitialDamage = info:GetDamage()

    vecSrc.z = vecSrc.z + 1 -- in case grenade is lying on the ground

    -- iterate on all entities in the vicinity
    for i, pEntity in ipairs(ents.FindInSphere(vecSrc, flRadius)) do
        -- we have to save whether or not the player is killed so we don't give credit
        -- for pre-dead players.
        local bWasAliveBeforeExplosion = false
        if pEntity:IsPlayer() then
            bWasAliveBeforeExplosion = pEntity:Alive()
        end

        local bIgnoreDamageToThisEntity = (pEntity:GetInternalVariable("m_takedamage") == 0) or (pEntity:IsPlayer() and not bWasAliveBeforeExplosion)

        if not bIgnoreDamageToThisEntity then
            vecSpot:Set(pEntity:WorldSpaceCenter())

            local bHit = false

            if bIgnoreWorld then
                vecEndPos:Set(vecSpot)
                bHit = true
            else
                -- get the percentage of the target entity that is visible from the
                -- explosion position.
                damagePercentage = swcs.GetAmountOfEntityVisible(vecSrc, pEntity)
                if damagePercentage > 0.0 then
                	vecEndPos = vecSpot
                	bHit = true
                end
            end

            if bHit then
                -- the explosion can 'see' this entity, so hurt them!
                vecToTarget:Set(vecEndPos)
                vecToTarget:Sub(vecSrc)

                -- use a Gaussian function to describe the damage falloff over distance, with flRadius equal to 3 * sigma
                -- this results in the following values:
                -- 
                -- Range Fraction  Damage
                --		0.0			100%
                -- 		0.1			96%
                -- 		0.2			84%
                -- 		0.3			67%
                -- 		0.4			49%
                -- 		0.5			32%
                -- 		0.6			20%
                -- 		0.7			11%
                -- 		0.8			 6%
                -- 		0.9			 3%
                -- 		1.0			 1%

                local fDist = vecToTarget:Length()
                local fSigma = flRadius / 3.0 -- flRadius specifies 3rd standard deviation (0.0111 damage at this range)
                local fGaussianFalloff = math.exp(-fDist * fDist / (2.0 * fSigma * fSigma))
                local flAdjustedDamage = flInitialDamage * fGaussianFalloff * damagePercentage

                if ( flAdjustedDamage > 0 ) then
                    local adjustedInfo = info
                    adjustedInfo:SetDamage( flAdjustedDamage )

                    local dir = vecToTarget
                    dir:Normalize()

                    -- If we don't have a damage force, manufacture one
                    if adjustedInfo:GetDamagePosition():IsZero() or adjustedInfo:GetDamageForce():IsZero() then
                        swcs.CalculateExplosiveDamageForce( adjustedInfo, dir, vecSrc, 1.5 --[[ explosion scale! ]] )
                    else
                        -- Assume the force passed in is the maximum force. Decay it based on falloff.
                        local flForce = adjustedInfo:GetDamageForce():Length() * falloff
                        adjustedInfo:SetDamageForce( dir * flForce )
                        adjustedInfo:SetDamagePosition( vecSrc )
                    end

                    local vecTarget = pEntity:WorldSpaceCenter()

                    util.TraceLine({
                        start = vecSrc,
                        endpos = vecTarget,
                        mask = MASK_SHOT,
                        filter = info:GetInflictor(),
                        collisiongroup = COLLISION_GROUP_NONE,
                        output = tr,
                    })

                    -- blasts always hit chest
                    tr.HitGroup = HITGROUP_GENERIC

                    if (tr.Fraction ~= 1.0) then
                        -- this has to be done to make breakable glass work.
                        --ClearMultiDamage( )
                        swcs.fx.TraceAttack(pEntity, adjustedInfo, dir, tr)
                        pEntity:DispatchTraceAttack(adjustedInfo, tr, dir)
                        --ApplyMultiDamage()
                    else
                        pEntity:TakeDamageInfo(adjustedInfo)
                    end

                    --print("trigger meme")

                    -- Now hit all triggers along the way that respond to damage... 
                    --pEntity:TraceAttackToTriggers( adjustedInfo, vecSrc, vecEndPos, dir )
                end
            end
        end
    end
end

local DENSITY_ABSORB_ALL_DAMAGE = 3000.0

-- return a multiplier that should adjust the damage done by a blast at position vecSrc to something at the position
-- vecEnd.  This will take into account the density of an entity that blocks the line of sight from one position to
-- the other.
--
-- this algorithm was taken from the HL2 version of RadiusDamage.
function swcs.GetExplosionDamageAdjustment(vecSrc, vecEnd, pEntityToIgnore)
    local retval = 0.0
    local tr = {}

    util.TraceLine({
        start = vecSrc,
        endpos = vecEnd,
        mask = MASK_SHOT,
        filter = pEntityToIgnore,
        collisiongroup = COLLISION_GROUP_NONE,
        output = tr
    })

    if tr.Fraction == 1.0 then
        retval = 1.0
    elseif not tr.HitWorld and tr.Entity ~= NULL and tr.Entity ~= pEntityToIgnore and tr.Entity:GetOwner() ~= pEntityToIgnore then
        -- if we didn't hit world geometry perhaps there's still damage to be done here.

        local blockingEntity, iSurfaceProps = tr.Entity
        if blockingEntity:IsValid() then
            iSurfaceProps = tr.SurfaceProps
        end

        -- check to see if this part of the player is visible if entities are ignored.
        util.TraceLine({
            start = vecSrc,
            endpos = vecEnd,
            mask = CONTENTS_SOLID,
            filter = NULL,
            collisiongroup = COLLISION_GROUP_NONE,
            output = tr
        })

        if tr.Fraction == 1.0 then
            if blockingEntity:IsValid() then
                local flDensity

                local surf_data = util.GetSurfaceData(iSurfaceProps)
                --if not surf_data then
                --    surf_data = util.GetSurfaceData()
                --end
                flDensity = surf_data.density

                local scale = flDensity / DENSITY_ABSORB_ALL_DAMAGE
                if ((scale >= 0.0) and (scale < 1.0)) then
                    retval = 1.0 - scale
                elseif scale < 0.0 then
                    -- should never happen, but just in case.
                    retval = 1.0
                end
            else
                retval = 0.75 -- we're blocked by something that isn't an entity with a physics model or world geometry, just cut damage in half for now.
            end
        end
    end

    return retval
end

local damagePercentageChest = 0.40
local damagePercentageHead = 0.20
local damagePercentageFeet = 0.20
local damagePercentageRightSide = 0.10
local damagePercentageLeftSide = 0.10

local HalfHumanWidth = 16
local HumanHeight = 71

-- returns the percentage of the player that is visible from the given point in the world.
-- return value is between 0 and 1.
function swcs.GetAmountOfEntityVisible(vecSrc, entity)
    local retval = 0.0

    if not entity:IsPlayer() then
        -- the entity is not a player, so the damage is all or nothing.
        local vecTarget = entity:WorldSpaceCenter()

        return swcs.GetExplosionDamageAdjustment(vecSrc, vecTarget, entity)
    end

    local ply = entity

    -- check what parts of the player we can see from this point and modify the return value accordingly.
    local chestHeightFromFeet

    local armDistanceFromChest = HalfHumanWidth

    -- calculate positions of various points on the target player's body
    local vecFeet = ply:GetPos()

    local vecChest = ply:WorldSpaceCenter()
    chestHeightFromFeet = vecChest.z - vecFeet.z  -- compute the distance from the chest to the feet. (this accounts for ducking and the like)

    local vecHead = ply:GetPos()
    vecHead.z = vecHead.z + HumanHeight

    local vecRightFacing = ply:GetAngles():Right()
    vecRightFacing:Normalize()
    vecRightFacing:Mul(armDistanceFromChest)

    local vecLeftSide = vecFeet
    vecLeftSide.x = vecLeftSide.x - vecRightFacing.x
    vecLeftSide.y = vecLeftSide.y - vecRightFacing.y
    vecLeftSide.z = vecLeftSide.z + chestHeightFromFeet

    local vecRightSide = vecFeet
    vecRightSide.x = vecRightSide.x + vecRightFacing.x
    vecRightSide.y = vecRightSide.y + vecRightFacing.y
    vecRightSide.z = vecRightSide.z + chestHeightFromFeet

    -- check chest
    local damageAdjustment = swcs.GetExplosionDamageAdjustment(vecSrc, vecChest, entity)
    retval = retval + (damagePercentageChest * damageAdjustment)

    -- check top of head
    damageAdjustment = swcs.GetExplosionDamageAdjustment(vecSrc, vecHead, entity)
    retval = retval + (damagePercentageHead * damageAdjustment)

    -- check feet
    damageAdjustment = swcs.GetExplosionDamageAdjustment(vecSrc, vecFeet, entity)
    retval = retval + (damagePercentageFeet * damageAdjustment)

    -- check left "edge"
    damageAdjustment = swcs.GetExplosionDamageAdjustment(vecSrc, vecLeftSide, entity)
    retval = retval + (damagePercentageLeftSide * damageAdjustment)

    -- check right "edge"
    damageAdjustment = swcs.GetExplosionDamageAdjustment(vecSrc, vecRightSide, entity)
    retval = retval + (damagePercentageRightSide * damageAdjustment)

    return retval
end

local ENTITY = FindMetaTable("Entity")
function ENTITY:SWCS_Alive()
    if self:IsPlayer() then
        return self:Alive()
    end

    local life_state = self:GetInternalVariable("m_lifestate")
    if life_state and life_state == 0 then
        return true
    end

    return false
end
function ENTITY:SetWaterLevel(nLevel)
    assert(isnumber(nLevel))
    self:SetSaveValue("m_nWaterLevel", nLevel)
end

function swcs.FormatViewModelAttachment(vOrigin, bFrom --[[= false]])
    local vEyePos = Vector()
    local aEyesRot = Angle()
    local flViewFOV = 0
    local flViewModelFOV = 0

    if game.SinglePlayer() then
        local ply = Entity(1)

        vEyePos:Set(ply:EyePos())
        aEyesRot:Set(ply:EyeAngles())

        flViewFOV = ply:GetFOV()
        flViewModelFOV = ply:GetActiveWeapon().ViewModelFOV
    elseif CLIENT then
        vEyePos:Set(EyePos())
        aEyesRot:Set(EyeAngles())

        local viewSetup = render.GetViewSetup()
        flViewFOV = viewSetup.fov_unscaled
        flViewModelFOV = viewSetup.fovviewmodel_unscaled
    end

    local vOffset = vOrigin - vEyePos
    local vForward = aEyesRot:Forward()

    local nViewX = math.tan(flViewModelFOV * math.pi / 360)

    if (nViewX == 0) then
        vForward:Mul(vForward:Dot(vOffset))
        vEyePos:Add(vForward)

        return vEyePos
    end

    local nWorldX = math.tan(flViewFOV * math.pi / 360)

    if (nWorldX == 0) then
        vForward:Mul(vForward:Dot(vOffset))
        vEyePos:Add(vForward)

        return vEyePos
    end

    local vRight = aEyesRot:Right()
    local vUp = aEyesRot:Up()

    local nFactor = bFrom and (nWorldX / nViewX) or (nViewX / nWorldX)

    vRight:Mul(vRight:Dot(vOffset) * nFactor)
    vUp:Mul(vUp:Dot(vOffset) * nFactor)
    vForward:Mul(vForward:Dot(vOffset))

    vEyePos:Add(vRight)
    vEyePos:Add(vUp)
    vEyePos:Add(vForward)

    return vEyePos
end

function swcs.ScaleFOVByAspectRatio(fovDegrees, ratio)
    local halfAngleRadians = fovDegrees * (0.5 * math.pi / 180.0)
    local halfTanScaled = math.tan(halfAngleRadians) * ratio
    return (180.0 / math.pi) * math.atan(halfTanScaled) * 2.0
end

-- returns a directional vector for a position on screen, corrects for mismatched fov
function swcs.ScreenToWorld(x, y)
    local view = render.GetViewSetup()
    local w, h = view.width, view.height
    local fov = view.fov_unscaled

    fov = swcs.ScaleFOVByAspectRatio(fov, (w / h) / (4 / 3))

    return util.AimVector(view.angles, fov, x, y, w, h)
end

-- transform in1 by the matrix in2
function swcs.VectorTransform(in1, in2, out)
    local in2Column0 = Vector(in2:GetField(1, 1), in2:GetField(1, 2), in2:GetField(1, 3))
    local in2Column1 = Vector(in2:GetField(2, 1), in2:GetField(2, 2), in2:GetField(2, 3))
    local in2Column2 = Vector(in2:GetField(3, 1), in2:GetField(3, 2), in2:GetField(3, 3))

    local x = in1:Dot(in2Column0) + in2:GetField(1, 4)
    local y = in1:Dot(in2Column1) + in2:GetField(2, 4)
    local z = in1:Dot(in2Column2) + in2:GetField(3, 4)

    if not out then
        return Vector(x, y, z)
    else
        out:SetUnpacked(x, y, z)
    end
end

-- August 16 to 23
function swcs.IsParty()
    --do return true end

    local t = os.date"!*t"

    return t.month == 8 and t.day >= 16 and t.day <= 23
end

local TICK_INTERVAL = engine.TickInterval()
local function TICK_TO_TIME(t)
    return t * TICK_INTERVAL
end

local function TIME_TO_TICK(t)
    return math.floor(t / TICK_INTERVAL)
end

local swcs_experm_interp = CreateConVar("swcs_experm_interp", "0", {FCVAR_ARCHIVE}, "enable experimental interpolation for the weapon pack's networked variables", 0, 1)
function swcs.DefineInterpolatedVar(tab, keyName, getSetterName, bIsDTVar)
    local strGetUninterpolated = "GetUninterpolated" .. getSetterName
    local strSetUninterpolated = "SetUninterpolated" .. getSetterName
    local strGetLast = "GetLast" .. getSetterName
    local strSetLast = "SetLast" .. getSetterName
    local strSet = "Set" .. getSetterName
    local strGet = "Get" .. getSetterName

    -- Get/Set Last val
    --tab[keyName .. "Last"] = Angle()
    tab[strGetLast] = function(self)
        return self[keyName .. "Last"]
    end
    tab[strSetLast] = function(self, ang)
        self[keyName .. "Last"] = ang
    end

    -- Get/Set Uninterpolated val
    if not bIsDTVar then
        --tab[keyName .. "Uninterpolated"] = Angle()
        tab[strGetUninterpolated] = function(self)
            return self[keyName .. "Uninterpolated"]
        end
        tab[strSetUninterpolated] = function(self, value)
            self[keyName .. "Uninterpolated"] = value
        end
    elseif bIsDTVar == true then
        tab[strGetUninterpolated] = tab[strGet]
        tab[strSetUninterpolated] = tab[strSet]
    end

    tab[strGet] = function(self, bInterpolated)
        if (SERVER or bInterpolated == false or game.SinglePlayer()) or not swcs_experm_interp:GetBool() then
            return self[strGetUninterpolated](self)
        end

        local flTimeNow = CurTime()
        local flClientTick = TICK_TO_TIME(TIME_TO_TICK(flTimeNow) + 1)
        local flBetweenTickPercentage = (flClientTick - flTimeNow) / TICK_INTERVAL

        local prevVal = self[strGetLast](self)
        local uninterpVal = self[strGetUninterpolated](self)

        local prevLerp = (prevVal * flBetweenTickPercentage)
        local uninterpLerp = (uninterpVal * (1.0 - flBetweenTickPercentage))

        local fullLerp = prevLerp + uninterpLerp

        return fullLerp
    end
    tab[strSet] = function(self, value)
        if IsFirstTimePredicted() then
            self[strSetLast](self, self[strGetUninterpolated](self))
        end

        self[strSetUninterpolated](self, value)
    end
end

local host_timescale = GetConVar"host_timescale"
function swcs.FrameTime()
    if CLIENT then
        local bPaused = FrameTime() == 0
        local flTimeScale = host_timescale:GetFloat()

        if bPaused then
            return 0
        end

        return RealFrameTime() * flTimeScale
    else
        return FrameTime()
    end
end

