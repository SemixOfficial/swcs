hook.Add( "AddToolMenuCategories", "source-weps", function()
	spawnmenu.AddToolCategory("Utilities", "swcs", "SWCS")
end)

hook.Add("PopulateToolMenu", "source-weps",function()

	-- sv toggles
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "sv_settings", "Serverside Settings", "", "", function(pnl)
		pnl:ControlHelp("These require you to be admin to change.")
		pnl:CheckBox("Sync bullet spread seed", "swcs_weapon_sync_seed")
		pnl:CheckBox("Enable nospread", "weapon_accuracy_nospread")
		pnl:CheckBox("Individual weapon ammo", "swcs_weapon_individual_ammo")
		pnl:CheckBox("Shotgun spread patterns", "weapon_accuracy_shotgun_spread_patterns")

		pnl:NumSlider("Recoil scale", "weapon_recoil_scale", 0, 10, 1)
		pnl:NumSlider("Show bullet impacts", "sv_showimpacts", 0, 3, 0)
		pnl:NumSlider("Deploy speed multiplier", "swcs_deploy_override", 0, 4, 1)
	end)

	-- clientside shit
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "xhair_settings", "Crosshair Settings", "", "", function(pnl)
		pnl:ControlHelp("Customize your crosshair.")

		local style = pnl:ComboBox("Style", "cl_crosshairstyle")
		style:AddChoice("default", 0)
		style:AddChoice("default (static)", 1)
		style:AddChoice("accurate (split)", 2)
		style:AddChoice("accurate (dynamic)", 3)
		style:AddChoice("classic (static)", 4)
		style:AddChoice("classic (dynamic)", 5)

		local cmb_color = pnl:ComboBox("Color", "cl_crosshaircolor")
		cmb_color:AddChoice("red", 0)
		cmb_color:AddChoice("green", 1)
		cmb_color:AddChoice("yellow", 2)
		cmb_color:AddChoice("blue", 3)
		cmb_color:AddChoice("cyan", 4)
		cmb_color:AddChoice("custom", 5)
		pnl:NumSlider("Alpha value", "cl_crosshairalpha", 0, 255, 0)
		pnl:CheckBox("Use alpha", "cl_crosshairusealpha")

		local colormix = vgui.Create("DColorMixer", pnl)
		colormix:SetConVarR("cl_crosshaircolor_r")
		colormix:SetConVarG("cl_crosshaircolor_g")
		colormix:SetConVarB("cl_crosshaircolor_b")
		colormix:SetAlphaBar(false)
		pnl:AddItem(colormix)

		pnl:CheckBox("Tee-style crosshair", "cl_crosshair_t")
		pnl:CheckBox("Center dot", "cl_crosshairdot")
		pnl:NumSlider("Pip thickness", "cl_crosshairthickness", 0, 20, 0)
		pnl:NumSlider("Pip size", "cl_crosshairsize", 0, 250, 0)
		pnl:NumSlider("Center gap", "cl_crosshairgap", 0, 250, 0)
		pnl:CheckBox("Use weapon gap value", "cl_crosshairgap_useweaponvalue")
	end)

	-- viewmodel settings
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "viewmodel", "Viewmodel Settings", "", "", function(pnl)
		pnl:ControlHelp("Affects how your viewmodel looks.")
		pnl:NumSlider("Offset X", "viewmodel_offset_x", -2.5, 2.5, 1)
		pnl:NumSlider("Offset Y", "viewmodel_offset_y", -2.5, 2.5, 1)
		pnl:NumSlider("Offset Z", "viewmodel_offset_z", -2.5, 2.5, 1)

		pnl:NumSlider("Recoil tracking", "viewmodel_recoil", 0.0, 1, 2)
		pnl:CheckBox("Use new headbob", "cl_use_new_headbob")
		pnl:NumSlider("Bob frequency", "cl_bobcycle", 0.1, 2.0, 2)
		pnl:NumSlider("Vertical bob amount", "cl_bobamt_vert", 0.1, 2.0, 1)
		pnl:NumSlider("Lateral bob amount", "cl_bobamt_lat", 0.1, 2.0, 1)
		pnl:NumSlider("Bob lower amount", "cl_bob_lower_amt", 5, 30, 1)

		--[[
			cl_viewmodel_shift_left_amt( "cl_viewmodel_shift_left_amt","1.5", FCVAR_ARCHIVE, "The amount the viewmodel shifts to the left when shooting accuracy increases.", true, 0.5, true, 2.0 );
			cl_viewmodel_shift_right_amt( "cl_viewmodel_shift_right_amt","0.75", FCVAR_ARCHIVE, "The amount the viewmodel shifts to the right when shooting accuracy decreases.", true, 0.25, true, 2.0 );
		]]
	end)

	-- hands
	spawnmenu.AddToolMenuOption("Utilities", "swcs", "hands", "Hands", "", "", function(pnl)
		pnl:ControlHelp("Affects how your hands look.")

		pnl:AddControl( "listbox", {label = "Hands", options = {
			["GMod Hands"] = {swcs_hands = 0},
			["Bare Hands"] = {swcs_hands = 1},
			["Anarchist Gloves"] = {swcs_hands = 2},
			["Ghost Hands"] = {swcs_hands = 3},
			["Bloodhound Gloves"] = {swcs_hands = 4},
			["Bloodhound Gloves (Broken Fang)"] = {swcs_hands = 5},
			["Bloodhound Gloves (Hydra)"] = {swcs_hands = 6},
			["Fingerless Gloves"] = {swcs_hands = 7},
			["Full-Finger Gloves"] = {swcs_hands = 8},
			["Leather Hand-Wrap Gloves"] = {swcs_hands = 9},
			["Hard-Knuckle Gloves"] = {swcs_hands = 10},
			["Hard-Knuckle Gloves (Black)"] = {swcs_hands = 11},
			["Hard-Knuckle Gloves (Blue)"] = {swcs_hands = 12},
			["Motorcycle Gloves"] = {swcs_hands = 13},
			["Slick Gloves"] = {swcs_hands = 14},
			["Specialist Gloves"] = {swcs_hands = 15},
			["Sporty Gloves"] = {swcs_hands = 16},
		}})

		pnl:AddControl( "slider", {label = "Skin Tone", command = "swcs_hands_skin", min = 0, max = 4})

		pnl:AddControl( "ComboBox", {label = "Sleeves", options = {
			["No Sleeves"] = {swcs_sleeves = 0},
			["Anarchist"] = {swcs_sleeves = 1},

			-- Balkan
			["Balkan (Old)"] = {swcs_sleeves = 2},
			["Balkan (New, variant 1)"] = {swcs_sleeves = 3},
			["Balkan (New, variant 2)"] = {swcs_sleeves = 4},
			["Balkan (New, variant 3)"] = {swcs_sleeves = 5},
			["Balkan (New, variant 4)"] = {swcs_sleeves = 6},

			-- Heavies
			["Heavy CT"] = {swcs_sleeves = 7},
			["Heavy T"] = {swcs_sleeves = 8},

			-- FBI
			["FBI (variant 1)"] = {swcs_sleeves = 9},
			["FBI (variant 2)"] = {swcs_sleeves = 10},
			["FBI (variant 3)"] = {swcs_sleeves = 11},
			["FBI (variant 4)"] = {swcs_sleeves = 12},

			-- hello ct
			["GIGN"] = {swcs_sleeves = 13},
			["GSG9"] = {swcs_sleeves = 14},
			["IDF"] = {swcs_sleeves = 15},

			-- DZ sleeve
			["Dangerzone Jumpsuit"] = {swcs_sleeves = 16},

			-- THE ROLEX
			["Gold Watch"] = {swcs_sleeves = 17},

			-- "Professional"
			["\"Professional\""] = {swcs_sleeves = 18},

			-- SAS
			["SAS"] = {swcs_sleeves = 19},
			["SAS Agent"] = {swcs_sleeves = 20},

			-- Separatist sleeve
			["Separatist"] = {swcs_sleeves = 21},

			-- Seal Team 6
			["Seal Team 6 (variant 1)"] = {swcs_sleeves = 22},
			["Seal Team 6 (variant 2)"] = {swcs_sleeves = 23},
			["Seal Team 6 (variant 3)"] = {swcs_sleeves = 24},
			["Seal Team 6 (variant 4)"] = {swcs_sleeves = 25},
			["Seal Team 6 (variant 5)"] = {swcs_sleeves = 26},
			["Seal Team 6 (variant 6)"] = {swcs_sleeves = 27},
			["Seal Team 6 (variant 7)"] = {swcs_sleeves = 28},
			["Seal Team 6 (variant 8)"] = {swcs_sleeves = 29},

			-- SWAT Team
			["SWAT (variant 1)"] = {swcs_sleeves = 30},
			["SWAT (variant 2)"] = {swcs_sleeves = 31},
			["SWAT (variant 3)"] = {swcs_sleeves = 32},

			-- i just got out the hospital
			["Wristband"] = {swcs_sleeves = 33},
		}})
	end)
end)
