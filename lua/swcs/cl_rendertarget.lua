local rt_Scope = GetRenderTarget("_rt_Scope", 1024, 1024)
local rt_viewmodel = GetRenderTarget("swcs_rt_vm", 2048, 2048)

hook.Add("PreDrawViewModels", "swcs.rt", function()
	render.CopyRenderTargetToTexture(rt_Scope)
end)

hook.Add("PostDrawViewModel", "swcs.rt", function()
	render.CopyRenderTargetToTexture(rt_viewmodel)
end)
