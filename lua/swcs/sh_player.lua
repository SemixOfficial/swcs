AddCSLuaFile()

local PLAYER = FindMetaTable "Player"

if CLIENT then
    local fov_desired = GetConVar "fov_desired"
    function PLAYER:GetDefaultFOV()
        return fov_desired:GetInt()
    end
else
    function PLAYER:GetDefaultFOV()
        return self:GetInternalVariable "m_iDefaultFOV"
    end
end

function PLAYER:Deafen(flDistance)
    -- Spectators don't get deafened
    if ( (self:GetObserverMode() == OBS_MODE_NONE )  or  (self:GetObserverMode() == OBS_MODE_IN_EYE ) ) then
        -- dsp presets are defined in hl2/scripts/dsp_presets.txt

        local effect

        if ( flDistance < 100 ) then
            effect = 35
        elseif ( flDistance < 500 ) then
            effect = 36
        elseif ( flDistance < 1000 ) then
            effect = 37
        else
            -- too far for us to get an effect
            return
        end

        self:SetDSP(effect, false)

        -- TODO: bots can't hear sound for a while?
    end
end

function PLAYER:Blind( holdTime, fadeTime, startingAlpha )
    -- Don't flash a spectator.
    local clr = Color(255, 255, 255, 255)

    clr.a = startingAlpha

    -- estimate when we can see again
    local oldBlindUntilTime = self:GetNWFloat("BlindUntilTime", 0)
    local oldBlindStartTime = self:GetNWFloat("BlindStartTime", 0)
    self:SetNWFloat("BlindUntilTime", math.max( self:GetNWFloat("BlindUntilTime", 0), CurTime() + holdTime + 0.5 * fadeTime ))
    self:SetNWFloat("BlindStartTime", CurTime())

    fadeTime = fadeTime / 1.4

    if ( CurTime() > oldBlindUntilTime ) then
        -- The previous flashbang is wearing off, or completely gone
        self:SetNWFloat("FlashDuration", fadeTime)
        self:SetNWFloat("FlashMaxAlpha", startingAlpha)
    else
        -- The previous flashbang is still going strong - only extend the duration
        local remainingDuration = oldBlindStartTime + self:GetNWFloat("FlashDuration", 0) - CurTime()

        local flNewDuration = math.max( remainingDuration, fadeTime )

        -- The flashbang client effect runs off a network var change callback... Make sure the bits for duration get
        -- sent by changing it a tiny bit whenever these end up being equal.
        if ( self:GetNWFloat("FlashDuration", 0) == flNewDuration ) then
            flNewDuration = flNewDuration + 0.01 end

        self:SetNWFloat("FlashDuration", flNewDuration)
        self:SetNWFloat("FlashMaxAlpha", math.max( self:GetNWFloat("FlashMaxAlpha", 0), startingAlpha ))
    end
end

function PLAYER:Unblind()
    self:SetNWFloat("FlashDuration", 0.0)
    self:SetNWFloat("FlashMaxAlpha", 0.0)
end

function PLAYER:ClearFlashbangScreenFade()
    if IsBlind() then
        local clr = Color( 0, 0, 0, 0)
        self:ScreenFade(bit.bor(SCREENFADE.OUT, SCREENFADE.PURGE), clr, 0.01, 0.0)

        self:SetNWFloat("FlashDuration", 0.0)
        self:SetNWFloat("FlashMaxAlpha", 255.0)
    end

    -- clear blind time (after screen fades are canceled )
    self:SetNWFloat("BlindUntilTime", 0.0)
    self:SetNWFloat("BlindStartTime", 0.0)
end

function PLAYER:GetLastWeapon()
    return self:GetInternalVariable("m_hLastWeapon")
end

function PLAYER:IsFlashBangActive()
    return ( self:GetNWFloat("FlashDuration", 0.0) > 0.0 ) and ( CurTime() < self:GetNWFloat("FlashBangTime", 0.0) )
end
function PLAYER:IsFlashBangBuildUpActive()
    return self:GetNWBool("FlashBuildUp", false) and self:IsFlashBangActive()
end
function PLAYER:GetFlashStartTime()
    return self:GetNWFloat("FlashBangTime", 0.0) - self:GetNWFloat("FlashDuration", 0.0)
end
function PLAYER:GetFlashTimeElapsed()
    return math.max( CurTime() - self:GetFlashStartTime(), 0.0 )
end

function PLAYER:IsBlinded()
    return (self:GetNWFloat("FlashBangTime", 0.0) - 1.0) > CurTime()
end

function PLAYER:UpdateFlashBangEffect()
    if self:GetNWFloat("FlashBangTime", 0.0) < CurTime() or self:GetNWFloat("FlashMaxAlpha", 0) <= 0.0 then
        -- FlashBang is inactive
        self:SetNWFloat("FlashScreenshotAlpha", 0.0)
        self:SetNWFloat("FlashOverlayAlpha", 0.0)
        return
    end

    local FLASH_BUILD_UP_PER_FRAME = 45.0
    local FLASH_BUILD_UP_DURATION = ( 255.0 / FLASH_BUILD_UP_PER_FRAME ) * ( 1.0 / 60.0 )

    local flFlashTimeElapsed = self:GetFlashTimeElapsed()

    if self:GetNWBool("FlashBuildUp", false) then
        -- build up
        self:SetNWFloat("FlashScreenshotAlpha", math.Clamp( ( flFlashTimeElapsed / FLASH_BUILD_UP_DURATION ) * self:GetNWFloat("FlashMaxAlpha", 0), 0.0, self:GetNWFloat("FlashMaxAlpha", 0) ))
        self:SetNWFloat("FlashOverlayAlpha", self:GetNWFloat("FlashScreenshotAlpha", 0.0))

        if flFlashTimeElapsed >= FLASH_BUILD_UP_DURATION then
            self:SetNWBool("FlashBuildUp", false)
        end
    else
        -- cool down
        local flFlashTimeLeft = self:GetNWFloat("FlashBangTime", 0.0) - CurTime()
        self:SetNWFloat("FlashScreenshotAlpha", ( self:GetNWFloat("FlashMaxAlpha", 0) * flFlashTimeLeft ) / self:GetNWFloat("FlashDuration", 0.0))
        self:SetNWFloat("FlashScreenshotAlpha", math.Clamp( self:GetNWFloat("FlashScreenshotAlpha", 0.0), 0.0, self:GetNWFloat("FlashMaxAlpha", 0) ))

        local flAlphaPercentage = 1.0
        local certainBlindnessTimeThresh = 3.0 -- yes this is a magic number, necessary to match CS/CZ flashbang effectiveness cause the rendering system is completely different.

        if (flFlashTimeLeft > certainBlindnessTimeThresh) then
            -- if we still have enough time of blindness left, make sure the player can't see anything yet.
            flAlphaPercentage = 1.0
        else
            -- blindness effects shorter than 'certainBlindness`TimeThresh' will start off at less than 255 alpha.
            flAlphaPercentage = flFlashTimeLeft / certainBlindnessTimeThresh

            -- reduce alpha level quicker with dx 8 support and higher to compensate
            -- for having the burn-in effect.
            flAlphaPercentage = flAlphaPercentage * flAlphaPercentage
        end

        flAlphaPercentage = flAlphaPercentage * self:GetNWFloat("FlashMaxAlpha", 0)
        self:SetNWFloat("FlashOverlayAlpha", flAlphaPercentage) -- scale a [0..1) value to a [0..MaxAlpha] value for the alpha.

        -- make sure the alpha is in the range of [0..MaxAlpha]
        self:SetNWFloat("FlashOverlayAlpha", math.max( self:GetNWFloat("FlashOverlayAlpha", 0.0), 0.0 ))
        self:SetNWFloat("FlashOverlayAlpha", math.min( self:GetNWFloat("FlashOverlayAlpha", 0.0), self:GetNWFloat("FlashMaxAlpha", 0)))
    end
end

function PLAYER:GetButtons()
    return self:GetInternalVariable("m_nButtons")
end
function PLAYER:SetButtons(buts)
    return self:SetInternalVariable("m_nButtons", buts)
end

function PLAYER:HasHelmet()
    return self:GetNWBool("SWCS.Helmet", false)
end
function PLAYER:GiveHelmet()
    self:SetNWBool("SWCS.Helmet", true)
end
function PLAYER:RemoveHelmet()
    self:SetNWBool("SWCS.Helmet", false)
end

local function setup_proxy(lp)
    lp:SetNWVarProxy("FlashDuration", function(ply, _, old, new)
        ply:SetNWBool("FlashBuildUp", false)

        local flNewFlashDuration = new
        if ( flNewFlashDuration == 0.0 ) then
            -- Disable flashbang effect
            ply:SetNWFloat("FlashScreenshotAlpha", 0.0)
            ply:SetNWFloat("FlashOverlayAlpha", 0.0)
            ply:SetNWBool("FlashBuildUp", false)
            ply.m_bFlashScreenshotHasBeenGrabbed = false
            ply:SetNWFloat("FlashBangTime", 0.0)
            --ply:SetNWBool("FlashDspHasBeenCleared", false)
            return
        end

        -- If local player is spectating in mode other than first-person, reduce effect duration by half
        --C_CSPlayer *pLocalPlayer = C_CSPlayer::GetLocalCSPlayer();
        --if ( pLocalPlayer && pLocalPlayer->GetObserverMode() != OBS_MODE_NONE && pLocalPlayer->GetObserverMode() != OBS_MODE_IN_EYE )
        --{
        --	flNewFlashDuration *= 0.5f;
        --}
        --

        if flNewFlashDuration > 0.0 and old == flNewFlashDuration then
            -- Ignore this update. This is a resend from the server
            return
        end

        if ( not ply:IsFlashBangActive() and flNewFlashDuration > 0.0 ) then
            -- reset flash alpha to start of effect build-up
            ply:SetNWFloat("FlashScreenshotAlpha", 1.0)
            ply:SetNWFloat("FlashOverlayAlpha", 1.0)
            ply:SetNWBool("FlashBuildUp", true)
            ply.m_bFlashScreenshotHasBeenGrabbed = false
        end

        --ply:SetNWFloat("FlashDuration", flNewFlashDuration)
        ply:SetNWFloat("FlashBangTime", CurTime() + flNewFlashDuration)
        ply:SetNWBool("FlashDspHasBeenCleared", false)
    end)

    -- hack to call the proxy above, setting up our flashbang
    timer.Simple(0, function()
        lp:SetNWFloat("FlashDuration", 0.01)
    end)
end

if CLIENT then
    if LocalPlayer():IsValid() then
        setup_proxy(LocalPlayer())
    else
        hook.Add("InitPostEntity", "swcs.nwvar_proxy", function()
            setup_proxy(LocalPlayer())
        end)
    end
elseif SERVER and game.SinglePlayer() then
    hook.Add("PlayerInitialSpawn", "swcs.nwvar_proxy", function(ply)
        setup_proxy(ply)
    end)
end

hook.Add("StartCommand", "swcs.cmd", function(ply, cmd)
    if cmd:CommandNumber() ~= 0 then
        ply.m_LastUserCommand = cmd
    end
end)
